# docker-postgres-pgadmin
## setup
```
git clone https://gitlab.di.unipmn.it/20041679/docker-postgres-pgadmin.git
cd docker-postgres-pgadmin
chmod 666 pgadmin/servers.json
```
## avvio
```
docker compose up -d
```
visitare http://localhost sul browser
## arresto
```
docker compose down
```
## log
```
docker compose logs -f
```
